package spring.dto;

import lombok.Getter;
import lombok.Setter;
import spring.entity.Comment;
import spring.entity.Post;
import spring.entity.Tag;

import java.util.stream.Collectors;

@Setter
@Getter
public class PostDto {

    private String title;
    private String content;
    private String tags;
    private String comments;

    public static PostDto fromPost(Post post){
        PostDto postDto = new PostDto();
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setTags(post.getTags().stream()
                .map(Tag::getName)
                .collect(Collectors.joining(" ")));
        postDto.setComments(post.getComments().stream()
                .map(Comment::getContent)
                .collect(Collectors.joining(" ")));

        return postDto;
    }

}
