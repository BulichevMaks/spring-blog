package spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.entity.Role;

import java.util.Optional;

public interface RolRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);

}
