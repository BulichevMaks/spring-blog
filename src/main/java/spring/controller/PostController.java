package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import spring.dto.PostDto;
import spring.entity.Post;
import spring.repository.PostRepository;
import spring.repository.TagRepository;
import spring.repository.UserRepository;
import spring.service.PostService;
import spring.service.TagService;
import spring.service.UserService;

import javax.servlet.ServletContext;
import java.util.Comparator;
import java.util.stream.Collectors;

import static spring.utils.SecurityUtils.checkAuthorityOnPost;

@Controller
public class PostController {

    public static final Sort SORT_BY_DT = Sort.by("dtCreated").descending();

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final TagRepository tagRepository;
    private final TagService tagService;
    private final PostService postService;
    private final ServletContext context;



    @Autowired
    public PostController(PostRepository postRepository, UserRepository userRepository, UserService userService, TagRepository tagRepository, TagService tagService, PostService postService, ServletContext context) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        this.userService = userService;
        this.tagRepository = tagRepository;
        this.tagService = tagService;
        this.postService = postService;
        this.context = context;
    }

    @GetMapping("/posts")
    public String posts(ModelMap model) {
        model.put("postList", postRepository.findAll());
        return "posts";
    }

    @GetMapping("/tags")
    public String tags(ModelMap model) {
        model.put("tagList", tagRepository.findAll());
        return "tags";
    }

    @GetMapping
    public String blog(ModelMap model, @RequestParam(required = false) String q) {
        if (StringUtils.hasText(q)) {
            model.put("title", "Search by");
            model.put("subtitle", q.length() > 20 ? q.substring(0, 20) + "..." : q);
            String pattern = "%" + q + "%";
            model.put("postList", postRepository.findByContentLikeIgnoreCase(pattern, SORT_BY_DT));
        } else {
            model.put("title", "Blog");
            model.put("subtitle", "All posts");
            model.put("postList", postRepository.findAll(SORT_BY_DT));
        }
        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/user/{username}")
    public String byUser(ModelMap model, @PathVariable String username) {
        model.put("title", "By user");
        model.put("subtitle", username);
        //  model.put("postList", postRepository.findByUser_Username(username));

        model.put("postList", userService.findByUsername(username).getPosts());

        setCommonParams(model);

        return "blog";
    }

    @GetMapping("/tag/{name}")
    public String byTag(ModelMap model, @PathVariable String name){
        model.put("title", "By tag");
        model.put("subtitle", name);
       // model.put("postList", postRepository.findSorted());
        model.put("postList", tagService
                .findByName(name)
                .getPosts()
                .stream()
                .sorted(Comparator.comparing(Post::getDtCreated).reversed())
                .collect(Collectors.toList()));

        setCommonParams(model);
        return "blog";
    }

    @GetMapping("/post/new")
    public String postNew(ModelMap model) {
        setCommonParams(model);
        return "post-new";
    }

    @PostMapping("/post/new")
    public String postNew(PostDto postDto){
        Post post = postService.create(postDto);
        return "redirect:/post/" + post.getPostId();
    }


    @GetMapping("/post/{postId}/edit")
    public String postEdit(@PathVariable long postId, ModelMap model){
        Post post = postService.findById(postId);
        checkAuthorityOnPost(post);
        model.put("postId", postId);
        model.put("post", PostDto.fromPost(post));
        setCommonParams(model);
        return "post-edit";
    }
    @PostMapping("/post/{postId}/edit")
    public String postEdit(@PathVariable long postId,
                           PostDto postDto){
        postService.update(postId, postDto);
        return "redirect:/post/" + postId;
    }


    @GetMapping("/post/{postId}")
    public String post(@PathVariable long postId, ModelMap model){
        Post post = postService.findById(postId);
        model.put("post", post);
        setCommonParams(model);
        return "post";
    }


    @PostMapping("/post/{postId}/delete")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable long postId){
        postService.delete(postId);
    }




    private void setCommonParams(ModelMap model) {
       // model.put("tagList", tagService.findAll());
        model.put("tagMap", tagService.tagPeriodicity());
        model.put("users", userRepository.findAll(SORT_BY_DT));
        model.put("contextPath", context.getContextPath());
    }
}
