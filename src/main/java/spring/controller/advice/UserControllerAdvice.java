package spring.controller.advice;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import spring.controller.UserController;

import javax.persistence.EntityExistsException;

@ControllerAdvice(assignableTypes = UserController.class)
public class UserControllerAdvice {

    @ExceptionHandler(EntityExistsException.class)
    public String handleEntityExistsException(Model model){
        model.addAttribute("error", "Username already exists");
        return "redirect:/sign-up";
    }


}
