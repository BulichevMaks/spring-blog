package spring.service;


import spring.entity.Tag;

import java.util.List;
import java.util.Map;


public interface TagService {

    void create(String name);

    void create(String... names);

    Tag findByName(String name);


    List<Tag> findAll();
    Map<Tag, Integer> tagPeriodicity();
}
