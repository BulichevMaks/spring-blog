package spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import spring.service.CommentService;

@Controller
public class CommentController {

    private final CommentService commentService;
    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }


    @PostMapping("/comment/create")
    public String create(long postId, String content) {
        commentService.create(postId, content);
        return "redirect:/post/" + postId;
    }
}
