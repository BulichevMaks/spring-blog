package spring.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.dto.PostDto;
import spring.service.PostService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.*;


@RestController
@RequestMapping("/api/post")
public class PostApiController {
    private final PostService postService;

    @Autowired
    public PostApiController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public ResponseEntity<List<PostDto>> findALl(){
        return ResponseEntity.ok(postService.findAll().stream()
                .map(PostDto::fromPost)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{postId}")
    public ResponseEntity<PostDto> findById(@PathVariable long postId){
        return ResponseEntity.ok(PostDto.fromPost(postService.findById(postId)));
    }

    @PostMapping()
    public ResponseEntity<PostDto> create(@RequestBody PostDto postDto){
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(PostDto.fromPost(postService.create(postDto)));
    }

    @PutMapping("/{postId}")
    public ResponseEntity<PostDto> update(@PathVariable long postId,
                                          @RequestBody PostDto postDto){
        return ResponseEntity.ok(PostDto.fromPost(postService.update(postId, postDto)));
    }

    @DeleteMapping("/{postId}")
    public ResponseEntity<Void> delete(@PathVariable long postId){
        postService.delete(postId);
        return noContent().build();
    }


}
