package spring.controller.advice;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import spring.entity.Tag;
import spring.repository.UserRepository;
import spring.service.TagService;

import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import static spring.controller.PostController.SORT_BY_DT;

@ControllerAdvice
public class NotFoundControllerAdvice {

    private final UserRepository userRepository;
    private final TagService tagService;

    public NotFoundControllerAdvice(UserRepository userRepository, TagService tagService) {
        this.userRepository = userRepository;
        this.tagService = tagService;
    }

    @ExceptionHandler(value = {NoSuchElementException.class,
            NoHandlerFoundException.class})
    public String handleNoSuchElement(Model model) {
        setCommonParams(model);
        return "404";
    }

    private void setCommonParams(Model model) {
        model.addAttribute("tags", tagService.findAll()
                .stream()
                .collect(Collectors.toMap(Tag::getName, tag -> tag.getPosts().size())));
        model.addAttribute("users", userRepository.findAll(SORT_BY_DT));
    }
}
