package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.entity.Post;
import spring.entity.Tag;
import spring.repository.PostRepository;
import spring.repository.TagRepository;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final PostRepository postRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, PostRepository postRepository) {
        this.tagRepository = tagRepository;
        this.postRepository = postRepository;
    }

    @Override
    public void create(String name) {
        Tag tag = new Tag();
        tag.setName(name);
        tagRepository.save(tag);
    }

    @Override
    public void create(String... names) {
        Arrays.stream(names).forEach(this::create);
    }

    @Override
    public Tag findByName(String name) {
        Tag tag = tagRepository.findByName(name).orElseThrow();
        tag.getPosts().size();
        return tag;
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findSortedByPostCount();
    }

    @Override
    public Map<Tag, Integer> tagPeriodicity() {
        Map<Tag, Integer> map = new HashMap<>();
        List<Tag> tags = tagRepository.findAll();
        for (Tag tag: tags) {
            map.put(tag, countPostByTag(tag.getName()));
        }
        return map;
    }


    public Integer countPostByTag(String tag) {
        return postRepository.findByTags_name(tag).size();
    }



}
