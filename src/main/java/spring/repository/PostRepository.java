package spring.repository;

import java.util.List;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import spring.entity.Comment;
import spring.entity.Post;


public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findByContentLikeIgnoreCase(String str, Sort sortByDt);
    Post findByTitle(String title);
    List<Post> findByUser_Username(String username);
    List<Post> findByTags_name(String name);

    //Find all posts sorted by tag count
    @Query(value = """
            select
            	p.*
            from
            	post p
            		natural join post_tag pt
            	group by p.post_id
            	order by count(*) desc
            """, nativeQuery = true)
    List<Post> findSortedByTagCount();
}
