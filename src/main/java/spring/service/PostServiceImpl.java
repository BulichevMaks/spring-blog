package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import spring.dto.PostDto;
import spring.entity.Post;
import spring.entity.Tag;
import spring.entity.User;
import spring.repository.PostRepository;
import spring.repository.TagRepository;
import spring.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static spring.utils.SecurityUtils.*;

@Service
@Transactional
public class PostServiceImpl implements PostService {

    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(TagRepository tagRepository,
                           UserRepository userRepository,
                           PostRepository postRepository) {
        this.tagRepository = tagRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @Override
    @Secured("ROLE_USER")
    public Post create(PostDto postDto) {
        Post post = new Post();
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setTags(parseTags(postDto.getTags()));

        User user = userRepository.findByUsername(
                getCurrentUserDetails().getUsername()).orElseThrow();
        post.setUser(user);
        post.setDtCreated(LocalDateTime.now());
        // checkAuthorityOnPost(post);
        return postRepository.save(post);
    }

    @Override
    public Post findById(long postId) {
        Post post = postRepository.findById(postId).orElseThrow();
        post.getTags().size();
        post.getComments().size();

        return post;
    }

    @Override
    //    @PreAuthorize("hasAnyRole('USER')")
    @Secured("ROLE_USER")
    public Post update(long postId, PostDto postDto) {
        Post post = postRepository.findById(postId).orElseThrow();
        checkAuthorityOnPost(post);

        if (StringUtils.hasText(postDto.getTitle())) {
            post.setTitle(postDto.getTitle());
        }

        if (StringUtils.hasText(postDto.getContent())) {
            post.setContent(postDto.getContent());
        }

        if (postDto.getTags() != null) {
            Set<Tag> newTags = parseTags(postDto.getTags());
            post.getTags().removeAll(newTags);
            removeUnusedTags(post);

            post.setTags(newTags);
        }


        post.setDtUpdated(LocalDateTime.now());
        return postRepository.save(post);
    }

    @Override
    public void delete(long postId) {
        Post post = postRepository.findById(postId).orElseThrow();
        checkIsAuthorOrAdmin(post);
        postRepository.deleteById(postId);
    }

    @Override
    public List<Post> findAll() {
        List<Post> posts =  postRepository.findAll();
        posts.forEach(p -> p.getTags().size());
        return posts;
    }



    private void removeUnusedTags(Post post) {
        Set<Tag> unusedTags = post.getTags().stream()
                .filter(t -> t.getPosts().size() == 1)
                .collect(Collectors.toSet());
        if(unusedTags.size() > 0) {
            tagRepository.deleteAll(unusedTags);
        }
    }


    private Set<Tag> parseTags(String tags) {
        if (!StringUtils.hasText(tags)) {
            return new HashSet<>();
        }

        return Arrays.stream(tags.split(" "))
                .filter(StringUtils::hasText)
                .map(tagName -> tagRepository
                        .findByName(tagName)
                        .orElseGet(() -> tagRepository.save(new Tag(tagName))))
                .collect(Collectors.toSet());
    }


}
