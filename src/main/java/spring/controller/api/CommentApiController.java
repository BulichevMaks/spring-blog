package spring.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.dto.CommentDto;
import spring.repository.CommentRepository;
import spring.service.CommentService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static spring.controller.PostController.SORT_BY_DT;

@RestController
@RequestMapping("/api/comment")
public class CommentApiController {

    private final CommentRepository commentRepository;
    private final CommentService commentService;


    @Autowired
    public CommentApiController(CommentRepository commentRepository,
                                CommentService commentService) {
        this.commentRepository = commentRepository;
        this.commentService = commentService;
    }

    @GetMapping
    public ResponseEntity<List<CommentDto>> findAll() {
        return ok(commentRepository.findAll(SORT_BY_DT)
                .stream()
                .map(CommentDto::fromComment)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{commentId}")
    public ResponseEntity<CommentDto> findById(@PathVariable Long commentId) {
        return ok(CommentDto.fromComment(commentService.findById(commentId)));
    }

    @PostMapping
    public ResponseEntity<CommentDto> create(@RequestBody CommentDto commentDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(CommentDto.fromComment(commentService.create(commentDto)));
    }

    @PutMapping("/{commentId}")
    public ResponseEntity<CommentDto> update(@PathVariable long commentId,
                                             @RequestBody CommentDto commentDto) {
        return ok(CommentDto.fromComment(commentService.update(commentId, commentDto)));
    }

    @DeleteMapping("/{commentId}")
    public ResponseEntity<Void> delete(@PathVariable long commentId) {
        commentService.delete(commentId);
        return noContent().build();
    }

}
