package spring.controller.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import spring.controller.PostController;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(assignableTypes = PostController.class)
public class PostControllerAdvice {

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public String handleMethodArgumentTypeMismatch(HttpServletRequest req, Exception e) throws Exception {
        return "redirect:/";
    }


}
