package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import spring.entity.User;
import spring.repository.RolRepository;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final spring.repository.UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RolRepository rolRepository;



    @Autowired
    public UserServiceImpl(spring.repository.UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, RolRepository rolRepository) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.rolRepository = rolRepository;
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepository.findByUsername(username).orElseThrow();
        user.getPosts().size();
        return user;
    }

    @Override
    public void create(String username, String password) {
        if (userRepository.findByUsername(username).isPresent()) {
            throw new EntityExistsException("This user already exists");
        } else {
            User user = new User();
            user.setUsername(username);
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user.setDtCreated(LocalDateTime.now());
            user.setActive(true);
            user.setRoles(List.of(rolRepository.findByName("USER").orElseThrow()));
            userRepository.save(user);
        }
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username).orElseThrow();
        user.getRoles().size();

        return user;
    }

}
