package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import spring.dto.CommentDto;
import spring.entity.Comment;
import spring.entity.Post;
import spring.entity.User;
import spring.repository.CommentRepository;
import spring.repository.PostRepository;
import spring.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static spring.utils.SecurityUtils.getCurrentUserDetails;

import static spring.utils.SecurityUtils.*;


@Service
@Transactional
public class CommentServiceImpl implements CommentService {
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;


    @Autowired
    public CommentServiceImpl(PostRepository postRepository,
                              CommentRepository commentRepository,
                              UserRepository userRepository) {
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void create(Long postId, String content) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setDtCreated(LocalDateTime.now());

        Post post = postRepository.findById(postId).orElseThrow();
        comment.setPost(post);

        User user = userRepository.findByUsername(
                getCurrentUserDetails().getUsername()).orElseThrow();
        comment.setUser(user);
        commentRepository.save(comment);
    }

    @Override
    public Comment findById(Long id) {
        return commentRepository.findById(id).orElseThrow();
    }

    @Override
    @Secured("ROLE_USER")
    public Comment create(CommentDto commentDto) {
        Comment comment = new Comment();
        if (StringUtils.hasText(commentDto.getContent())) {
            comment.setContent(commentDto.getContent());
        }
        UserDetails details = getCurrentUserDetails();
        comment.setDtCreated(LocalDateTime.now());
        comment.setUser(userRepository
                .findByUsername(details.getUsername())
                .orElseThrow());

        comment.setPost(postRepository
                .findById(commentDto.getPostId())
                .orElseThrow());

        return commentRepository.save(comment);
    }

    @Secured("ROLE_USER")
    @Override
    public Comment update(Long commentId, CommentDto commentDto) {
        Comment comment = commentRepository.findById(commentId).orElseThrow();
        checkAuthorityOnComment(comment);

        if (StringUtils.hasText(commentDto.getContent())) {
            comment.setContent(commentDto.getContent());
        }


        comment.setDtUpdated(LocalDateTime.now());
        return commentRepository.save(comment);

    }

    @Override
    public void delete(long commentId) {
        Comment comment = commentRepository.findById(commentId).orElseThrow();
        checkAuthorityOnComment(comment);
        commentRepository.deleteById(commentId);
    }



}
