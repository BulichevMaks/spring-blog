package spring.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/sign-in")
    public String signIn(ModelMap model,
                         @RequestParam(required = false) String error){
        if (error != null) {
            model.put("error", "Bad Credentials");
        }
        return "sign-in";
    }


    @GetMapping("/sign-up")
    public String signUp(ModelMap model,
                         @RequestParam(required = false) String error){
        model.put("error", error);
        return "sign-up";
    }


    @PostMapping("/sign-up")
    public String signUp(String username, String password){
        userService.create(username, password);
        return "redirect:/sign-in";
    }
}
