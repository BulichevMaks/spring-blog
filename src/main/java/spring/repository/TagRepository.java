package spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import spring.entity.Post;
import spring.entity.Tag;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {
    Optional<Tag> findByName(String name);

    @Query(value = """
              select
            	t.*
            from
            	tag t
            		natural join post_tag pt
            	group by t.tag_id
            	order by count(*) desc
            """, nativeQuery = true)
    List<Tag> findSortedByPostCount();

}
