drop TABLE IF EXISTS comment;
drop TABLE IF EXISTS post_tag;
drop TABLE IF EXISTS tag;
drop TABLE IF EXISTS post;

drop TABLE IF EXISTS user_role;
drop TABLE IF EXISTS role;
drop TABLE IF EXISTS users;

--ACID

create table users (
    user_id bigserial primary key,
    username varchar(50) not null unique,
    password varchar(200) not null,
    dt_created timestamp not null,
    is_active boolean default false
);


create table role(
    role_id bigserial primary key,
    name VARCHAR(50) NOT NULL
);

CREATE TABLE user_role (
    user_id BIGINT REFERENCES users(user_id) ON DELETE CASCADE NOT NULL,
    role_id bigint references role(role_id) on delete cascade not null,
    primary key (user_id, role_id)
);


create table post (
    post_id bigserial primary key,
    user_id bigint references users(user_id) on delete cascade not null,
    title varchar(100) not null,
    content text not null,
    dt_created timestamp not null,
    dt_updated timestamp
);

create table tag (
    tag_id bigserial primary key,
    name varchar(50) NOT NULL
);


CREATE TABLE post_tag (
    post_id bigint REFERENCES post(post_id) ON DELETE CASCADE NOT NULL,
    tag_id bigint references tag(tag_id) on delete cascade not null,
    primary key (post_id, tag_id)
);

create table comment (
    comment_id bigserial primary key,
    post_id bigint references post(post_id) on delete cascade not null,
    user_id bigint references users(user_id) on delete cascade not null,
    content text,
    dt_created timestamp not null,
    dt_updated timestamp
);




--Data

insert into role(name) values ('ADMIN');
insert into role(name) values ('USER');

insert into users (username, password, dt_created, is_active)
    values ('admin', '$2a$10$SUhysIAfzq1Xs19kdet6zuPiXvXxiPqt4C14kWs/clzkgjp80Vo2q', now()::timestamp, true);

insert into users (username, password, dt_created, is_active)
    values ('user1', '$2a$10$GDZdF6M6.J4tW1Y1sCYMeuWwFfOzqqVo2of1dmNmbLk8b6BpqfnAe', now()::timestamp, true);

insert into users (username, password, dt_created, is_active)
    values ('user2', '$2a$10$DUR5DH3LsADknFrv2LMh1OuP5gq2prorwJKhKcjpYoKZKZVYRb4w2', now()::timestamp, true);


insert into user_role(user_id, role_id) values (1, 1);

insert into user_role(user_id, role_id) values (2, 2);
insert into user_role(user_id, role_id) values (3, 2);




insert into post (user_id, title, content, dt_created, dt_updated)
	values (2, 'Day 1', 'It''s all good!', current_timestamp - interval '2 days', null);
insert into post (user_id, title, content, dt_created, dt_updated)
	values (2, 'Day 2', 'It''s all ok!', current_timestamp - interval '1 days', null);
insert into post (user_id, title, content, dt_created, dt_updated)
	values (3, 'Day 3', 'It''s all bad!', current_timestamp, null);


insert into tag (name) values ('news');
insert into tag (name) values ('life');
insert into tag (name) values ('day');
insert into tag (name) values ('mood');
insert into tag (name) values ('ideas');
insert into tag (name) values ('thoughts');

insert into post_tag(post_id, tag_id) values (1, 1);
insert into post_tag(post_id, tag_id) values (1, 2);
insert into post_tag(post_id, tag_id) values (2, 3);
insert into post_tag(post_id, tag_id) values (2, 2);
insert into post_tag(post_id, tag_id) values (2, 1);
insert into post_tag(post_id, tag_id) values (2, 5);
insert into post_tag(post_id, tag_id) values (3, 3);
insert into post_tag(post_id, tag_id) values (3, 2);
insert into post_tag(post_id, tag_id) values (3, 6);

insert into comment (user_id, post_id, content, dt_created)
    values (2, 2, 'Nice!', current_timestamp);
insert into comment (user_id, post_id, content, dt_created)
    values (1, 1, 'Awesome!', current_timestamp);
insert into comment (user_id, post_id, content, dt_created)
    values (1, 1, 'Excellent!', current_timestamp);
insert into comment (user_id, post_id, content, dt_created)
    values (2, 1, 'Wonderful!', current_timestamp);
insert into comment (user_id, post_id, content, dt_created)
    values (2, 3, 'Disgusting!', current_timestamp);
insert into comment (user_id, post_id, content, dt_created)
    values (1, 3, 'Atrocious!', current_timestamp);

select * from users;

  select
            	p.*
            from
            	post p
            		natural join post_tag pt
            	group by p.post_id
            	order by count(*) desc

  select
            	t.*, count(*)
            from
            	tag t
            		natural join post_tag pt
            	group by t.tag_id
            	order by count(*) desc

  select
            	content
            from
            	comment where post_id = 3

select * from comment;
select * from users;