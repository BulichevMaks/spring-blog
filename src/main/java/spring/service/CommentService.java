package spring.service;

import spring.dto.CommentDto;
import spring.entity.Comment;

public interface CommentService {
    void create(Long postId, String content);

    Comment findById(Long commentId);

    Comment create(CommentDto commentDto);

    Comment update(Long commentId, CommentDto commentDto);

    void delete(long postId);


}
