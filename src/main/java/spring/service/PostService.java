package spring.service;

import spring.dto.PostDto;
import spring.entity.Post;

import java.util.List;

public interface PostService {
    Post create(PostDto postDto);
    Post findById(long postId);

    Post update(long postId, PostDto postDto);

    void delete(long postId);

    List<Post> findAll();

}
